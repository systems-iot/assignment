# Getting started

Make sure to set up your `virtualenv` before writing any code or starting on this step. To set up your virtual environment, make sure to install virtualenv via `pip`:

```
pip install virtualenv
```

> This is not done in a virtualenv!

Then create your virtualenv in your `step-3` folder:

```
virtualenv venv
```

and activate it:

```
source venv/bin/activate
```

> On Windows, you will need to activate your virtualenv with the `bat` file located in `venv/Scripts/activate.bat`

Once active, you should see a `(venv)` at the start of your command prompt.

Once your env is active, install the dependencies:

```
pip install -r requirements.txt
```

You are good to go!

## Importing TraCI

In order for `controller.py` to import `traci`, you will need to make sure that your `SUMO_HOME` path is set:

```
export SUMO_HOME=/usr/local/Cellar/sumo/1.2.0/share/sumo/
```

> On Windows, you will need to use the `set`: `set SUMO_HOME=C:\Program Files (x86)\Eclipse\Sumo`

## Running the python script

To control SUMO, you will need to run the python script (make sure you have activated your virtual environment):

```
python controller.py
```

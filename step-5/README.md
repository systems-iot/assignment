# Executing commands in Iotery for your vehicle

One of the challenges of an IoT system is keeping your cloud and things in sync. A significant amount of coordination and communication is required to make sure that all your systems remain working together.

The following is aimed to give you a some ideas on how to have your vehicle "execute" a command, and notify Iotery about it.

When this command is executed:

```
iotery_response_delivery_vehicle = delivery_vehicle_cloud_connector.postData(
        deviceUuid=delivery_vehicle["uuid"], data=data)
```

Iotery will respond with a RESTful JSON response that looks somethingn like this:

```json
{
  ...
  "unexecutedCommands": {
    "device": [
      {
        "uuid": "commandInstanceUuid",
        "deviceUuid": "deviceUuid",
        "commandTypeUuid": "commandTypeUuid",
        "batchReferenceUuid": null,
        "name": null,
        "timestamp": 1564519186,
        "isUnexecuted": true,
        "setExecutedTimestamp": null,
        "_data": []
      }
    ],
    "network": []
  },
  ...
}
```

Notice that the device has a single unexecuted command with uuid `commandInstanceUuid`. Once the device executes this command, it needs to tell Iotery that it has done so. To do this, use the delivery truck connector to tell Iotery that it has been executed:

```python
delivery_vehicle_cloud_connector.setCommandInstanceAsExecuted(commandInstanceUuid=commandInstanceUuid, data={"timestamp":int(time())})
```

where the `commandInstanceUuid` is the `uuid` field in the `unexecutedCommands`.
